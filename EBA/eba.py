import matplotlib.pyplot as plt
from io import BytesIO
import numpy as np
import pandas as pd
import datetime
from flask import *
from pytz import timezone
import math
import base64
from ...main import app
from ...api.utils import token_required, format_response
from ...core.influxdb import GetAsset, GetGroupByAssets, Q
from ...core.atria_sites import SITES, MF_POWER_CURVE

timezoneIndia = timezone('Asia/Kolkata')
timezoneUTC = timezone('UTC')

TAGS = {
    'BLYTH': [
        'mean("[POS_NAC]") as "Nacelle Position"',
        'mean("[T_AMB]") as "Ambient Temperature"',
        'mean("[P_ACT]") as "Power"',
        'last("[TurbineStatusSCADA]") as "Status"',
        'mean("[V_WIN]") as "Wind Speed"'
    ],
    'GVPALLI': [
        'mean("[POS_NAC]") as "Nacelle Position"',
        'mean("[T_AMB]") as "Ambient Temperature"',
        'mean("[P_ACT]") as "Power"',
        'last("[TurbineStatusSCADA]") as "Status"',
        'mean("[V_WIN]") as "Wind Speed"'
    ],
    'CHITRADURGA': [
        'mean("[POS_NAC]") as "Nacelle Position"',
        'mean("[T_AMB]") as "Ambient Temperature"',
        'mean("[P_ACT]") as "Power"',
        'last("[TurbineStatusSCADA]") as "Status"',
        'mean("[V_WIN]") as "Wind Speed"'
    ],
    'HIRIYUR': [
        'mean("[Inv_AI, Tot_Act_pwr_KW]")/10 as "Power"',
    ],
    'KABALI': [
        'mean("[Inv_AI, Tot_Act_pwr_KW]")/10 as "Power"',
    ],
    'KOGHALI': [
        'mean("[Inv_AI, Tot_Act_pwr_KW]")/10 as "Power"',
    ]
}


def getOneDayTQ():
    nowTZ = (datetime.datetime.now(timezoneIndia))
    now = nowTZ.strftime('%Y-%m-%d %H:%M:%S')
    today = nowTZ - \
        datetime.timedelta(
            hours=nowTZ.hour, minutes=nowTZ.minute, seconds=nowTZ.second)
    today_start = (today.astimezone(timezoneUTC)).strftime('%Y-%m-%d %H:%M:%S')
    yesterday = today - datetime.timedelta(days=1)
    yesterday_start = yesterday.astimezone(
        timezoneUTC).strftime('%Y-%m-%d %H:%M:%S')

    timeQuery = "time > '%s'  AND time < '%s' " % (
        yesterday_start, today_start)
    return timeQuery


def GetDataframe(*args):
    param = []
    for arg in args:
        param.append(arg)

    timeQuery = param[0]
    siteId = param[1]
    tags = param[2]
    duration = param[3]

    dataset = GetGroupByAssets(siteId, timeQuery, tags, duration)
    return dataset


def GetAllData(siteId, timeQuery, duration):
    siteConfig = SITES[siteId]
    tags = TAGS[siteId]
    return GetDataframe(timeQuery, siteConfig['siteId'], tags, duration)


def generate_excel_report(df, siteId):
    table = pd.pivot_table(df, index=['WTG', 'TimeStamp'],
                           values=["Wind Speed", "Speed_Power_Curve", "Nacelle Position", "Ambient Temperature", "Power",
                                   "Expected_Power", '_Diff'],
                           fill_value=0)
    table = table.reindex(columns=(["Wind Speed", "Speed_Power_Curve", "Nacelle Position", "Ambient Temperature", "Power",
                                    "Expected_Power", '_Diff']))
    excelfilename = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime(
        "%Y-%m-%d") + "(" + siteId + ")" + ".xlsx"
    writer = pd.ExcelWriter(excelfilename)

    for manager in table.index.get_level_values(0).unique():
        temp_df = table.xs(manager, level=0)
        temp_df.to_excel(writer, manager)

    writer.save()
    return table


def getBin(x, power_curve):
    speed = round((x*2), 0)/2
    index = int(speed*2)
    if index < 6 or index == 40:
        return speed, 0
    else:
        current_value = power_curve["POWER"][index]
        next_value = power_curve["POWER"][index+1]
        next_speed = speed + 0.5
        POWER = 1
        e_power = (next_value - current_value) / \
            (math.pow(next_speed, POWER) - math.pow(speed, POWER))
        e_power = current_value + e_power*math.pow(x - speed, POWER)
        return speed, e_power


def calculateDA(df):
    # print(df.to_csv())
    df = df['Power']
    null_sum = df.isnull().sum().sum()
    length_df = len(df)
    data_available = (length_df - null_sum) / length_df
    return data_available


def calculateContinuousIndex(dataset, column, getStateFunc, cutoff=2):
    last_sample = dataset[column][dataset.index[0]]
    current_state = getStateFunc(last_sample)
    current_state_start = dataset.index[0]
    current_state_end = dataset.index[0]
    current_state_count = 1

    for i in dataset.index:
        new_sample = dataset[column][i]
        new_state = getStateFunc(new_sample)

        if not new_state == current_state:
            if current_state_count >= cutoff and current_state == True:
                print(str(current_state) + " -> From " +
                      str(current_state_start) + " to " + str(current_state_end))
                return current_state, current_state_start, current_state_end
            current_state = new_state
            current_state_start = i
            current_state_end = i
            current_state_count = 1
        else:
            current_state_count = current_state_count + 1
            current_state_end = i


def getState(x):
    return x == True

def getNominalPerf(x):
    return x <=2.5
 
def powerGreaterThanZero(x):
    return x > 0

def powerLessThanZero(x):
    return x < 0
    

def getLowPower(x):
    return x > 2.5

def getStatus(x):
    return x == 2

def figureToPng(fig):
    # run plt.plot, plt.title, etc.
    figfile = BytesIO()
    fig.savefig(figfile, format='png')
    figfile.seek(0)  # rewind to beginning of file
    figdata_png = base64.b64encode(figfile.getvalue()).decode("utf-8")
    return figdata_png

@app.route("/sites/turbine_wise_generation")
def turbine_wise_generation():
    timeQuery = getOneDayTQ()
    siteId = request.args.get(
        "siteId") if request.args.get("siteId") else "BLYTH"
    duration = "10m"
    MODEL = SITES[siteId]['make']
    PC = MF_POWER_CURVE[MODEL]
    df = GetAllData(siteId, timeQuery, duration)
    da = calculateDA(df)
    df = df.dropna()
    df.index.name = 'TimeStamp'
    result = df['Wind Speed'].apply(
        getBin, power_curve=(PC))
    df['Speed_Power_Curve'] = [x[0] for x in result]
    df['Expected_Power'] = [x[1] for x in result]
    df['_Diff'] = 100*(df['Expected_Power'] - df['Power']) / \
        df['Expected_Power']
    current_state = []
    current_state_start = []
    current_state_end = []
    turbine_name = []
    for i in df['WTG'].unique():
        print(i)
        df_wtg = df[df['WTG'] == i]
        df_wtg = df_wtg.sort_values(by='TimeStamp')
        result = calculateContinuousIndex(
            df_wtg, '_Diff', getLowPower, cutoff=6)
        print("------------------------------")
        if(result):
            turbine_name.append(i)
            current_state.append(result[0])
            current_state_start.append(result[1])
            current_state_end.append(result[2])
    df_null = pd.DataFrame([current_state, current_state_start,
                            current_state_end], columns=list(turbine_name))

    total_power = df['Power'].sum() / 6
    expected_total_power = df['Expected_Power'].sum() / 6
    eba = str(100 - df['_Diff'].mean()) + "%"

    df_underperformance = df[df['_Diff'] > 10].copy()
    df_wtg_01 = df[df['WTG'] == 'WTG-01'].copy()
    df_wtg_02 = df[df['WTG'] == 'WTG-02'].copy()
    df_wtg_03 = df[df['WTG'] == 'WTG-03'].copy()
    df_wtg_04 = df[df['WTG'] == 'WTG-04'].copy()
    df_wtg_05 = df[df['WTG'] == 'WTG-05'].copy()
    df_wtg_06 = df[df['WTG'] == 'WTG-06'].copy()
    df_wtg_07 = df[df['WTG'] == 'WTG-07'].copy()
    df_wtg_08 = df[df['WTG'] == 'WTG-08'].copy()


    df_performance = df['Power'].copy()
    # df_performance['Expected_Power'] = df['Expected_Power']
    print(df_performance)

    overall_df = pd.DataFrame([total_power, expected_total_power, da, eba])
    pivot_table = generate_excel_report(df_underperformance, siteId)
    # return render_template('turbine_wise.html', dfs = {'pivot': pivot_table, 'wtg01': df_wtg_01 },
    # figs= {
        # 'wtg01': figureToPng(df_wtg_01['Power'].plot().get_figure()),
        # 'wtg02': figureToPng(df_wtg_02['Power'].plot().get_figure()),
        # 'wtg03': figureToPng(df_wtg_03['Power'].plot().get_figure()),
        # 'wtg04': figureToPng(df_wtg_04['Power'].plot().get_figure()),
        # 'wtg05': figureToPng(df_wtg_05['Power'].plot().get_figure()),
        # 'wtg06': figureToPng(df_wtg_06['Power'].plot().get_figure()),
        # 'wtg07': figureToPng(df_wtg_07['Power'].plot().get_figure()),
        # 'wtg08': figureToPng(df_wtg_08['Power'].plot().get_figure()),
        # 'perf' : figureToPng( df_performance.plot().get_figure() )
    #     }
    
    # )
    return pivot_table.to_html()


@app.route("/sites/site_overview")
def site_overview():
    timeQuery = getOneDayTQ()
    siteId = request.args.get(
        "siteId") if request.args.get("siteId") else "BLYTH"
    duration = "1h"
    MODEL = SITES[siteId]['make']
    PC = MF_POWER_CURVE[MODEL]
    df = GetAllData(siteId, timeQuery, duration)
    df = df.dropna()
    df.index.name = 'TimeStamp'
    result = df['Wind Speed'].apply(
        getBin, power_curve=(PC))
    df['Speed_Power_Curve'] = [x[0] for x in result]
    df['Expected_Power'] = [x[1] for x in result]
    # pivot_table = generate_excel_report(df, siteId)
    df_new = df.groupby('Speed_Power_Curve').aggregate(
        {'Power': np.mean, 'Wind Speed': np.mean, 'Expected_Power': np.mean})
    df_new.loc['Total'] = pd.Series(
        [df_new['Expected_Power'].sum(), df_new['Wind Speed'].mean(), df_new['Power'].sum()], index=['Expected_Power', 'Wind Speed', 'Power'])
    return df_new.to_html()


@app.route("/sites/site_overall_performance")
def site_wise_generation():
    timeQuery = getOneDayTQ()
    siteId = request.args.get(
        "siteId") if request.args.get("siteId") else "BLYTH"
    duration = "10m"
    MODEL = SITES[siteId]['make']
    PC = MF_POWER_CURVE[MODEL]
    df = GetAllData(siteId, timeQuery, duration)
    da = calculateDA(df)
    df = df.dropna()
    df.index.name = 'TimeStamp'
    result = df['Wind Speed'].apply(
        getBin, power_curve=(PC))
    df['Speed_Power_Curve'] = [x[0] for x in result]
    df['Expected_Power'] = [x[1] for x in result]
    df['_Diff'] = 100*(df['Expected_Power'] - df['Power']) / \
        df['Expected_Power']
    total_power = df['Power'].sum() / 6
    expected_total_power = df['Expected_Power'].sum() / 6
    eba = str(100 - df['_Diff'].mean()) + "%"
    df_underperformance = df[df['_Diff'] > 10].copy()
    overall_df = pd.DataFrame([total_power, expected_total_power, da, eba], index=[
                              'Total_Power', 'Expected_Total_Power', 'DA', 'Eba'])
    return overall_df.to_html()


@app.route("/sites/turbine_wise_analysis")
def turbine_wise_analysis():
    timeQuery = getOneDayTQ()
    siteId = request.args.get(
        "siteId") if request.args.get("siteId") else "BLYTH"
    duration = "10m"
    MODEL = SITES[siteId]['make']
    PC = MF_POWER_CURVE[MODEL]
    df = GetAllData(siteId, timeQuery, duration)
    df.index.name = 'TimeStamp'

    dataset = pd.DataFrame()
    current_state = []
    current_state_start = []
    current_state_end = []
    turbine_name = []
    duration_mins = []

    for i in df['WTG'].unique():
        print(i)
        df_wtg = df[df['WTG'] == i]
        tempDF = pd.DataFrame()
        tempDF = df_wtg.sort_values(by='TimeStamp')
        result = calculateContinuousIndex(tempDF.isnull(), 'Power', getState)
        print("------------------------------")
        turbine_name.append(i)
        current_state.append(result[0])
        current_state_start.append(result[1])
        current_state_end.append(result[2])
        dataset = dataset.append(tempDF)
        date_format = "%H:%M:%S"
        st = result[1].strftime('%H:%M:%S')
        et = result[2].strftime('%H:%M:%S')
        start_time  = datetime.datetime.strptime(st, date_format)
        end_time  = datetime.datetime.strptime(et, date_format)
        durations = end_time - start_time
        minutes = (durations.seconds) / 60
        duration_mins.append(str(minutes))

        df_null = pd.DataFrame([current_state, current_state_start, current_state_end, duration_mins], columns=list(
            turbine_name), index=["State", "Start Time", "End Time", "Duration(mins)"])

    return df_null.transpose().to_html()

